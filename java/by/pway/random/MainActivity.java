package by.pway.random;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final Random random = new Random();
    ImageView ivWinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.btnGenerate);
        button.setOnClickListener(this);
        ivWinner = (ImageView) findViewById(R.id.ivWinner);
        ivWinner.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGenerate:
                String winner = "w" + (random.nextInt(13) + 1);
                ivWinner.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(winner, "drawable", getPackageName())));
                break;
            case R.id.ivWinner:
                ivWinner.setImageDrawable(null);
                break;
        }


    }
}